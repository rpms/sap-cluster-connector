#
# All modifications and additions to the file contributed by third parties
# remain the property of their copyright owners, unless otherwise agreed
# upon. The license for this file, and modifications and additions to the
# file, is the same license as for the pristine package itself (unless the
# license for the pristine package is not an Open Source License, in which
# case the license is the MIT License). An "Open Source License" is a
# license that conforms to the Open Source Definition (Version 1.9)
# published by the Open Source Initiative.
#

# Below is the script used to generate a new source file
# from the sap_cluster_connector upstream git repo.
#
# TAG=$(git log --pretty="format:%h" -n 1)
# distdir="sap_cluster_connector-${TAG}"
# TARFILE="${distdir}.tar.gz"
# rm -rf $TARFILE $distdir
# git archive --prefix=$distdir/ HEAD | gzip > $TARFILE
#

%global sap_script_prefix sap_cluster_connector
%global sap_script_hash f3644f5

Name:		sap-cluster-connector
Summary:	SAP cluster connector script
Version:	3.0.1
Release:	5%{?rcver:%{rcver}}%{?numcomm:.%{numcomm}}%{?alphatag:.%{alphatag}}%{?dirty:.%{dirty}}%{?dist}
License:	GPLv2+
URL:		https://github.com/fdanapfel/sap_cluster_connector
%if 0%{?fedora} || 0%{?centos_version} || 0%{?rhel}
Group:		System Environment/Base
%else
Group:		Productivity/Clustering/HA
%endif
Source0:	%{sap_script_prefix}-%{sap_script_hash}.tar.gz
Patch0: 	bz1827096-sap-cluster-connector-allow-dashes-underscores-in-nodenames.patch

BuildArch:	noarch

Requires:	resource-agents-sap >= 4.1.1-28
Requires:	perl

%description
The SAP connector script interface with Pacemaker to allow SAP
instances to be managed in a cluster environment.

%prep
%setup -q -n %{sap_script_prefix}-%{sap_script_hash}
%patch0 -p1

%build

%install
test -d %{buildroot}/%{_bindir} || mkdir -p %{buildroot}/%{_bindir}
mkdir -p %{buildroot}/%{_datadir}/sap_cluster_connector
mkdir -p %{buildroot}/%{_mandir}/man8
cp redhat/sap_cluster_connector %{buildroot}/%{_bindir}
cp -rv redhat/{run_checks,checks} %{buildroot}/%{_datadir}/sap_cluster_connector
gzip redhat/man/*.8
cp redhat/man/*.8.gz %{buildroot}/%{_mandir}/man8

%clean
rm -rf %{buildroot}

%files
%defattr(-,root,root)
%{_bindir}/sap_cluster_connector
%{_mandir}/man8/sap_cluster_connector*
%{_datadir}/sap_cluster_connector

%changelog
* Thu Apr 30 2020 Oyvind Albrigtsen <oalbrigt@redhat.com> - 3.0.1-5
- sap-cluster-connector: allow dashes/underscores in nodenames

  Resolves: rhbz#1827096

* Tue Jun 25 2019 Oyvind Albrigtsen <oalbrigt@redhat.com> - 3.0.1-4
- Initial build as separate package

  Resolves: rhbz#1688346
